const makeDoubleDigit = (stringNumber) => {
    return stringNumber.length === 1 ? '0' + stringNumber : stringNumber
}

export const parseToMinutesDisplay = (secondsDuration) => {
    let minutes = Math.floor(secondsDuration / 60).toString();
    let seconds = (secondsDuration % 60).toString();
    return `${makeDoubleDigit(minutes)}:${makeDoubleDigit(seconds)}`;
}