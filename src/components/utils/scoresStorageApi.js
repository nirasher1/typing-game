import { default as gameConfig } from '../game/gameConfig';


const TOP_SCORES_KEY = 'topScores'
const maxScoresCount = gameConfig.scoresCount;

let topScores = null;

export const getTopScores = () => topScores ? [...topScores] : []

const compareScores = (scoreA, scoreB) => {
    if (scoreA.duration > scoreB.duration) {
        return 1;
    }
    if (scoreA.duration < scoreB.duration) {
        return -1;
    }
    if (scoreA.date > scoreB.date) {
        return 1;
    }
    return -1;
}

const fetchTopScores = () => {
    try {
        return JSON.parse(window.localStorage.getItem(TOP_SCORES_KEY))
    } catch {
        return [];
    }
}

const putTopScores = () => {
    window.localStorage.setItem(TOP_SCORES_KEY, JSON.stringify(topScores))
}

export const insertNewScore = (duration) => {
    topScores.push({
        duration,
        date: new Date().toISOString()
    })
    topScores.sort(compareScores);
    topScores = topScores.slice(0, maxScoresCount)
    putTopScores();
}

const initTopScores = () => {
    topScores = fetchTopScores();
    if (!topScores) {
        topScores = []
        putTopScores();
    }
}

initTopScores();