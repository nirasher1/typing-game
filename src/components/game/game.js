import React, { useState } from 'react';
import WordsMonitor from './wordsMonitor/wordsMonitor';
import WordsInput from "./wordsInput/wordsInput";
import wordStatus from './wordStatus';
import useStopWatch from "./stopWatch/useStopWatch";
import { parseToMinutesDisplay } from "../utils/timeUtils";
import {  insertNewScore } from "../utils/scoresStorageApi";
import "./game.css"


const Game = (props) => {
    const [currentWordIndex, setCurrentWordIndex] = useState(0);
    const [currentWordStatus, setCurrentWordStatus] = useState(wordStatus.waiting);
    const [durationSeconds, start, stop, reset] = useStopWatch();

    const handleSuccess = () => {
        setCurrentWordStatus(wordStatus.waiting);
        if (currentWordIndex < props.words.length - 1) {
            setCurrentWordIndex(prevIndex => prevIndex + 1);
        } else {
            finishGame();
        }
    }

    const handleFail = () => {
        setCurrentWordStatus(wordStatus.failed);
    }

    const handleContain = () => {
        setCurrentWordStatus(wordStatus.waiting);
    }

    const handleFirstTyping = () => {
        props.setIsGameInProgress(true)
        reset()
        start()
    }

    const finishGame = () => {
        setCurrentWordIndex(null);
        insertNewScore(durationSeconds);
        props.setIsGameInProgress(false);
        stop();
    }

    const resetGame = () => {
        setCurrentWordStatus(wordStatus.waiting);
        setCurrentWordIndex(0);
        props.setIsGameInProgress(false);
        reset();
    }

    return (
        <div className="game">
            <div className="game-content">
                <WordsMonitor words={props.words}
                              currentWordIndex={currentWordIndex}
                              currentWordStatus={currentWordStatus}
                />
                <WordsInput className="words-input"
                            expectedWord={props.words[currentWordIndex]}
                            onFirstTyping={handleFirstTyping}
                            onSuccess={handleSuccess}
                            onContain={handleContain}
                            onNotContain={handleFail}
                            isGameInProgress={props.isGameInProgress}
                />
            </div>
            <div className="control-panel">
                <div className="stop-watch">
                    {parseToMinutesDisplay(durationSeconds)}
                </div>
                <button className="reset-button"
                        onClick={resetGame}
                >
                    Reset
                </button>
            </div>
        </div>
    )
}

export default Game;
