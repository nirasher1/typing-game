import React from 'react';
import {parseToMinutesDisplay} from "../../utils/timeUtils";
import "./topScoresList.css"


const TopScoresList = (props) => {
    return (
        <div className="top-scores">
            <span className="top-scores-title">Top {props.scoresCount}</span>
            {props.data && props.data.length ? (
                <ul className="scores-list">
                    {props.data.map((scoreDetails, scoreIndex) => (
                        <li className="score-item" key={scoreIndex}>
                            <span className="duration-info">{parseToMinutesDisplay(scoreDetails.duration)}</span>
                            <span className="date-info">- score from {new Date(scoreDetails.date).toLocaleString()}</span>
                        </li>
                    ))}
                </ul>
                ) : 'No Scores Found'

            }
        </div>
    )
}

export default TopScoresList;
