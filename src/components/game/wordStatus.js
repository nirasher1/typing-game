const wordStatus = {
    succeeded: "succeeded",
    failed: "failed",
    waiting: "waiting",
}

export default wordStatus;
