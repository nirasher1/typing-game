import React, {useEffect, useState} from 'react';


const WordsInput = (props) => {
    const [inputValue, setInputValue] = useState(null);

    useEffect(() => {
        if (!props.isGameInProgress) {
            setInputValue(null)
        }
    }, [props.isGameInProgress])

    const handleChange = (event) => {
        const newValue = event.target.value.trimStart();
        if (newValue) {
            checkForFirstTyping();
            handleNotEmptyValue(newValue);
        } else {
            setInputValue('')
            props.onContain();
        }
    }

    const handleNotEmptyValue = (newValue) => {
        if (newValue.trim() === props.expectedWord && newValue.slice(-1) === ' ') {
            props.onSuccess();
            setInputValue('');
        } else {
            if (props.expectedWord.startsWith(newValue.trim())) {
                props.onContain();
            } else {
                props.onNotContain();
            }
            setInputValue(newValue.trim());
        }
    }

    const checkForFirstTyping = () => {
        if (!props.isGameInProgress && !inputValue) {
            props.onFirstTyping();
            setInputValue('');
        }
    }

    return (
        <input className={props.className}
               type="input"
               value={inputValue || ''}
               onChange={handleChange}
               autoComplete="off"
               disabled={!props.expectedWord}
        />
    )
}

export default WordsInput;
