import { useState, useRef } from 'react';


const useStopWatch = () => {
    const [secondsDuration, setSecondsDuration] = useState(0);
    const measurementInterval = useRef(null)

    const start = () => {
        measurementInterval.current = setInterval(updateDuration, 1000);
    }

    const stop = () => {
        clearInterval(measurementInterval.current);
    }

    const reset = () => {
        stop();
        measurementInterval.current = null;
        setSecondsDuration(0);
    }

    const updateDuration = () => {
        setSecondsDuration(prevDuration => prevDuration + 1);
    }

    return [secondsDuration, start, stop, reset]
}

export default useStopWatch;
