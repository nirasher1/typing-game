import React from 'react';
import './wordsMonitor.css'
import wordStatus from "../wordStatus";


const getClassNamesByIndex = (currentWordIndex, wordToRenderIndex, currentWordStatus) => {
    const WORD_CLASSNAME = 'word';
    if (wordToRenderIndex > currentWordIndex) {
        return WORD_CLASSNAME + ' ' + wordStatus.waiting
    }
    if (wordToRenderIndex < currentWordIndex) {
        return WORD_CLASSNAME + ' ' + wordStatus.succeeded
    }
    if (wordToRenderIndex === currentWordIndex) {
        return WORD_CLASSNAME + ' ' + currentWordStatus + ' current-word'
    }
    return WORD_CLASSNAME
}

const WordsMonitor = (props) => {
    return (
        <div className="words-monitor">
            {props.words.map((word, wordIndex) => (
                <span key={wordIndex}
                      className={getClassNamesByIndex(
                          props.currentWordIndex,
                          wordIndex,
                          props.currentWordStatus)}
                >
                    {word}
                </span>
            ))}
        </div>
    )
}

export default WordsMonitor;
