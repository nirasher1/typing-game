import React, { useState, useEffect } from 'react';
import Game from './components/game/game';
import { default as gameConfig } from './components/game/gameConfig';
import { getTopScores } from './components/utils/scoresStorageApi'
import TopScoresList from "./components/game/topScoresList/topScoresList";
import './App.css';

function App() {
  const [topScores, setTopScores] = useState(getTopScores())
  const [isGameInProgress, setIsGameInProgress] = useState(false);

  useEffect(() => {
      if (!isGameInProgress) {
          setTopScores(getTopScores())
      }
  }, [isGameInProgress])

  return (
    <div className="page">
      <Game words={gameConfig.words}
            isGameInProgress={isGameInProgress}
            setIsGameInProgress={setIsGameInProgress}
      />
      <TopScoresList scoresCount={gameConfig.scoresCount}
                     data={topScores}
      />
    </div>
  );
}

export default App;
